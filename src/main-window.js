class CommonRollsMainWindow extends FormApplication{
	    constructor(...args) {
        super(...args)
        game.users.apps.push(this)
    }

    static get defaultOptions() {
        const options = super.defaultOptions;
        options.title = "";
        options.id = "common-rolls";
        options.template = "modules/common-rolls/templates/main-window.html";
        options.closeOnSubmit = true;
        options.popOut = true;
        options.width = 300;
        options.height = "auto";
        return options;
    }

    async getData() {
        // Return data to the template
        const isGM = game.user.isGM;
        const multiple = this.options.multiple;
        const actorData = this.options.actorData;

        return {
        	isGM,
        	multiple,
        	actorData
        };
    }

    activateListeners(html) {
        super.activateListeners(html);
        this.element.find("select[name=rolls]").change(this._onRollChange.bind(this));
        this.element.find("button[name=rollAllButton]").click(() => this.element.find("input[name=rollAllInput]").val("true"))
        this._onRollChange();
    }

    _onRollChange(){
    	let roll = this.element.find("select[name=rolls]").val();
    	
    	switch (roll) {
    		case "Perception":
    		    this.element.find("div[id=Perception]").show()
	    		this.element.find("select[name=Skills]").parent().hide();
	    		this.element.find("select[name=Abilities]").parent().hide();
	    		this.element.find("select[name=Saves]").parent().hide();
	    		break;    		
    		case "Skills":
    			this.element.find("div[id=Perception]").hide()
	    		this.element.find("select[name=Skills]").parent().show();
	    		this.element.find("select[name=Abilities]").parent().hide();
	    		this.element.find("select[name=Saves]").parent().hide();
	    		break;
	    	case "Abilities":
    			this.element.find("div[id=Perception]").hide()
	    		this.element.find("select[name=Skills]").parent().hide();
	    		this.element.find("select[name=Abilities]").parent().show();
	    		this.element.find("select[name=Saves]").parent().hide();
	    		break;
	    	case "Saves":
    			this.element.find("div[id=Perception]").hide()	    	
	    		this.element.find("select[name=Skills]").parent().hide();
	    		this.element.find("select[name=Abilities]").parent().hide();
	    		this.element.find("select[name=Saves]").parent().show();
	    		break;
    	}
    }

   _updateObject(event, formData) {
   		CommonRolls.processRoll(formData, this.options.tokens);
    }

    setData(tokens, multiple) {
    	if (!multiple) {
   			this.options.title = `Common Rolls [${tokens[0].name}]`;
   		} else {
   			this.options.title = `Common Rolls [Multiple]`;
   		}

		const actor_data = tokens[0].actor.data.data;
		let data = [];

		for (let [key, values] of Object.entries(actor_data)) {
			switch (key) {
				case "abilities":
					data[key] = this._getActorData(key, values, "PF2E.Ability");
					break;
				case "saves":
					data[key] = this._getActorData(key, values, "PF2E.Saves");
					break;
				case "skills":
					data[key] = this._getActorData(key, values, "PF2E.Skill");
					break;
				case "attributes":
					let n = game.i18n.localize("PF2E.PerceptionLabel");
				    data["perception"] = {name: n, value: values.perception.value, key: "perception"};
				    break;
			}
		}
		this.options.actorData = data;
   		this.options.multiple = multiple;
   		this.options.tokens = tokens;
    }

    _getActorData(key, values, localization_string) {
    	let d = [];

    	for (let [name, data] of Object.entries(values)) {
    		let n = name.charAt(0).toUpperCase() + name.slice(1);
    		n = game.i18n.localize(`${localization_string}${n}`);
    		let v = data.value;
    		if (key === "abilities") v = data.mod;
    		d.push({name: n, value: v, key: name})
    	}

    	return d;
    }
}