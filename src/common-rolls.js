class CommonRolls{
	static show() {
		const tokens = canvas.tokens.controlled
		if (tokens.length > 0){
			CommonRolls.showMainWindow(tokens)
		}else{
			ui.notifications.warn("Select at least one Token!")
		}
	}

	static showMainWindow(tokens){
		this.mainWindow = this.mainWindow || new CommonRollsMainWindow();

		if (tokens.length > 1) {
			this.mainWindow.setData(tokens, true);
		}else{
			this.mainWindow.setData(tokens, false);
		}
		this.mainWindow.render(true);
	}

	static processRoll(formData, tokens)
	{
		const rollMode = game.settings.get('core', 'rollMode');
		const key = formData.rolls;
		const name = formData[key];
		let modifier = formData.modifier;

        let chatMessages = []
        for (let token of tokens) {
        	let value;
			let formula = "1d20";
			let n;
			let flavor;

        	switch (key) {
        		case "Abilities":
		    		value = token.actor.data.data.abilities[name].mod;
		    		n = name.charAt(0).toUpperCase() + name.slice(1);
    				n = game.i18n.localize(`PF2E.Ability${n}`);
    				flavor = `${n} Ability Check`;
		    		break;
		    	case "Skills":
		    		value = token.actor.data.data.skills[name].value;
		    		n = name.charAt(0).toUpperCase() + name.slice(1);
    				n = game.i18n.localize(`PF2E.Skill${n}`);
		    		flavor = `${n} Skill Check`;
		    		break;
		    	case "Saves":
		    		value = token.actor.data.data.saves[name].value;
		    		n = name.charAt(0).toUpperCase() + name.slice(1);
		    		n = game.i18n.localize(`PF2E.Saves${n}`);
		    		flavor = `${n} Saving Throw`;
		    		break;
		    	case "Perception":
		    		value = token.actor.data.data.attributes.perception.value;
		    		n =	game.i18n.localize(`PF2E.${key}Label`);
		    		flavor = `${n} Check`;
		    		break;
        	}

        	if (modifier === "" || isNaN(modifier) || !modifier) modifier = 0;

        	if (modifier != 0)
        		formula = `${formula} + ${value} + ${modifier}`;
        	else
        		formula = `${formula} + ${value}`;

            let chatData = {
              user: game.user._id,
              speaker: ChatMessage.getSpeaker({token}),
              content: formula,
              flavor: flavor || null,
              type: CONST.CHAT_MESSAGE_TYPES.ROLL
            };
            try {
                let data = duplicate(token.data);
                data["name"] = token.name;
                let roll = new Roll(formula, data).roll();
                chatData.roll = JSON.stringify(roll);
                chatData.sound = CONFIG.sounds.dice;
            } catch(err) {
                chatData.content = `Error parsing the roll formula: ${formula}`
                chatData.roll = null;
                chatData.type = CONST.CHAT_MESSAGE_TYPES.OOC;
            }

			if ( ["gmroll", "blindroll"].includes(rollMode) ) chatData.whisper = ChatMessage.getWhisperIDs("GM");
			if ( rollMode === "selfroll" ) chatData.whisper = [game.user._id];
			if ( rollMode === "blindroll" ) chatData.blind = true;

            chatMessages.push(chatData);
        }
        ChatMessage.create(chatMessages, {rollMode: rollMode});	    
	}

	static getSceneControlButtons(buttons) {
		let tokenButton = buttons.find(b => b.name == "token")

		if (tokenButton) {
			tokenButton.tools.push({
				name: "common-rolls",
				title: "Common Rolls",
				icon: "fas fa-dice-d20",
				visible: true,
				button: true,
				onClick: () => CommonRolls.show()
			});
		}
	}
}
Hooks.on('getSceneControlButtons', CommonRolls.getSceneControlButtons)